// Menggunakan ESP32, Anda harus menyertakan pustaka berikut:
#include <Arduino.h>

// Tentukan pin yang akan digunakan untuk mengontrol LED
const int ledPin = 13;

void setup() {
  // Konfigurasi pin sebagai OUTPUT
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // Nyalakan LED
  digitalWrite(ledPin, HIGH);
  delay(1000); // Tunggu 1 detik

  // Matikan LED
  digitalWrite(ledPin, LOW);
  delay(1000); // Tunggu 1 detik lagi
}
