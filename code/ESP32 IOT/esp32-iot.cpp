// Menggunakan Arduino, Anda harus menyertakan pustaka berikut:
#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>

// Pengaturan WiFi
const char* ssid = "KKPI"; // Nama jaringan WiFi
const char* password = "sepertibiasa123"; // Kata sandi jaringan WiFi

// Pengaturan MQTT Broker
const char* mqtt_server = "192.168.100.48"; 
const int mqtt_port = 1890; 

// Pengaturan Topik (topic) MQTT
const char* lampu_topic = "lampu_kontrol"; // Topik untuk mengontrol lampu

// Pin ESP32 yang terhubung ke lampu
const int pin_lampu = GPIO_NUM_25; // Nomor pin yang digunakan untuk mengontrol lampu
const int pin_lampu_1 = GPIO_NUM_26; // Nomor pin yang digunakan untuk mengontrol lampu


// Koneksi WiFi
WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Pesan masuk dari topik: ");
  Serial.println(topic);
  Serial.print("Konten pesan: ");

  String message = "";
  for (int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  Serial.println(message);

  // Mematikan lampu
  if (message.equals("mati")) {
    digitalWrite(pin_lampu, LOW);
    digitalWrite(pin_lampu_1, LOW);
  }
  // Menghidupkan lampu
  else if (message.equals("hidup")) {
    digitalWrite(pin_lampu, HIGH);
    digitalWrite(pin_lampu_1, HIGH);
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Menghubungkan ke MQTT Broker...");
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);

    if (client.connect(clientId.c_str())) {
      Serial.println("Berhasil terhubung ke MQTT Broker");
      client.subscribe(lampu_topic);
    } else {
      Serial.print("Gagal, rc=");
      Serial.print(client.state());
      Serial.println(" Ulangi dalam 5 detik...");
      delay(5000);
    }
  }
}

void setup() {
  pinMode(pin_lampu, OUTPUT); // Mengatur pin sebagai OUTPUT
  pinMode(pin_lampu_1, OUTPUT); // Mengatur pin sebagai OUTPUT

  Serial.begin(115200); // Inisialisasi Serial monitor dengan baud rate 115200
  setup_wifi(); // Mengatur koneksi WiFi
  client.setServer(mqtt_server, mqtt_port); // Mengatur alamat dan port MQTT broker
  client.setCallback(callback); // Mengatur callback yang akan dipanggil ketika ada pesan masuk
}

void loop() {
  if (!client.connected()) {
    reconnect(); // Jika koneksi MQTT terputus, coba hubungkan kembali
  }
  client.loop(); // Proses loop untuk menghandle pesan MQTT yang masuk
}
