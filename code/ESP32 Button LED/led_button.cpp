// Menggunakan Arduino, Anda harus menyertakan pustaka berikut:
#include <Arduino.h>

// Tentukan pin yang akan digunakan untuk push button dan LED
const int buttonPin = 2;
const int ledPin = 13;

// Variabel untuk menyimpan status sebelumnya dari push button
int previousButtonState = LOW;

void setup() {
  // Konfigurasi pin sebagai INPUT untuk push button dan OUTPUT untuk LED
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // Baca status push button
  int buttonState = digitalRead(buttonPin);

  // Cek apakah push button ditekan
  if (buttonState == HIGH && previousButtonState == LOW) {
    // Tombol baru saja ditekan
    // Ubah status LED
    digitalWrite(ledPin, !digitalRead(ledPin));
  }

  // Simpan status terakhir dari push button
  previousButtonState = buttonState;
}
