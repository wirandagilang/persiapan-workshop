# persiapan-workshop

## WiFi
Username: `smk`
Password: `okebanget777`

## Software

### Arduino software
```
https://www.arduino.cc/en/software
```

### Simulation software 
```
https://wokwi.com
```
```
https://www.tinkercad.com
```

### ESP32 Json 
```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
```

### Driver USB Silabs
```
https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers?tab=downloads
```


## Project Contoh
```
https://www.youtube.com/shorts/xi4pzfPTeZI
```


## Latihan

### Blinking LED
```
https://www.youtube.com/shorts/savZODDR4pw
```

